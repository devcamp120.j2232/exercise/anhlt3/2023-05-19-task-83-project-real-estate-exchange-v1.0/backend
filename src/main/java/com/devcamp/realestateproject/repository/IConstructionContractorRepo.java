package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.ConstructionContractor;

public interface IConstructionContractorRepo extends JpaRepository<ConstructionContractor, Integer> {
    
}
