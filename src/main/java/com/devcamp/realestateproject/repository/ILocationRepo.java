package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Location;

public interface ILocationRepo extends JpaRepository<Location, Integer> {
    
}
