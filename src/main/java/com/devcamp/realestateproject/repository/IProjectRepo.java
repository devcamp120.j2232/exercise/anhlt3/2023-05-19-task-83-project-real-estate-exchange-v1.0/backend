package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Project;

public interface IProjectRepo extends JpaRepository<Project, Integer> {
    
}
