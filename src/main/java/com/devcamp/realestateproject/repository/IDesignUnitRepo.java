package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.DesignUnit;

public interface IDesignUnitRepo extends JpaRepository<DesignUnit, Integer> {
    
}
