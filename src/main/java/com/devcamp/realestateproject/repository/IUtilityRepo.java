package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Utility;

public interface IUtilityRepo extends JpaRepository<Utility, Integer> {
    
}
