package com.devcamp.realestateproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestateproject.model.District;

public interface IDistrictRepo extends JpaRepository<District, Integer> {
    @Query("SELECT DISTINCT d.prefix FROM District d WHERE d.prefix IN ('Quận', 'Huyện')")
    List<String> findAllPrefix();

    List<District> findByProvinceId_Id(int provinceId);
}
