package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.RegionLink;

public interface IRegionLinkRepo extends JpaRepository<RegionLink, Integer> {
    
}
