package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Customer;

public interface ICustomerRepo extends JpaRepository<Customer, Integer> {
    
}
