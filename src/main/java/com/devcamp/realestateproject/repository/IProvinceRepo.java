package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Province;

public interface IProvinceRepo extends JpaRepository<Province, Integer> {
    
}
