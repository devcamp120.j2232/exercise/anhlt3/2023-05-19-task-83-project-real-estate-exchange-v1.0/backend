package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.MasterLayout;

public interface IMasterLayoutRepo extends JpaRepository<MasterLayout, Integer> {
    
}
