package com.devcamp.realestateproject.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestateproject.model.Street;

public interface IStreetRepo extends JpaRepository<Street, Integer> {
    @Query("SELECT DISTINCT d.prefix FROM Street d WHERE d.prefix IN ('Đường', 'Phố')")
    List<String> findAllPrefix();

    Page<Street> findAll(Pageable pageable);
}
