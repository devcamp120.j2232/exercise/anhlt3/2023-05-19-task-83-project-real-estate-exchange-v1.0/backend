package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.RealEstate;

public interface IRealEstateRepo extends JpaRepository<RealEstate, Integer> {
    
}
