package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.AddressMap;

public interface IAddressMapRepo extends JpaRepository<AddressMap, Integer> {
    
}
