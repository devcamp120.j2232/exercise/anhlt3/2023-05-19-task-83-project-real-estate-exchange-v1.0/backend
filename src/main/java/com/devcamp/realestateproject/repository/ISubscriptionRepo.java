package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Subscription;

public interface ISubscriptionRepo extends JpaRepository<Subscription, Integer> {
    
}
