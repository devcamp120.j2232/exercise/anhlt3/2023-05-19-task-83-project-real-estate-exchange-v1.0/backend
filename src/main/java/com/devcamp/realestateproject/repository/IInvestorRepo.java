package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Investor;

public interface IInvestorRepo extends JpaRepository<Investor, Integer> {
    
}
