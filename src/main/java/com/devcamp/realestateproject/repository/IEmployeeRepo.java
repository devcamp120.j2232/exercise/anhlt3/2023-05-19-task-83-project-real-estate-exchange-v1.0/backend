package com.devcamp.realestateproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateproject.model.Employee;

public interface IEmployeeRepo extends JpaRepository<Employee, Integer> {
    
}
