package com.devcamp.realestateproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestateproject.model.Ward;

public interface IWardRepo extends JpaRepository<Ward, Integer> {
    @Query("SELECT DISTINCT d.prefix FROM Ward d WHERE d.prefix IN ('Phường', 'Xã', 'Thị trấn')")
    List<String> findAllPrefix();
}
