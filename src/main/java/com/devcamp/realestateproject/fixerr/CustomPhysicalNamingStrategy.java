package com.devcamp.realestateproject.fixerr;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class CustomPhysicalNamingStrategy extends PhysicalNamingStrategyStandardImpl {

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        if (name != null) {
            String columnName = name.getText();
            if ("EmployeeID".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("LastName".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("FirstName".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Title".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("TitleOfCourtesy".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("BirthDate".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("HireDate".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Address".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("City".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Region".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("PostalCode".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Country".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("HomePhone".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Extension".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Photo".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Notes".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("ReportsTo".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Username".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Password".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Email".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Activated".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("Profile".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("UserLevel".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("FSBO".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("DTSXD".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("CLCL".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("CTXD_price".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
            if ("CTXD_value".equalsIgnoreCase(columnName)) {
                return Identifier.toIdentifier(columnName);
            }
        }
        return super.toPhysicalColumnName(name, context);
    }
}