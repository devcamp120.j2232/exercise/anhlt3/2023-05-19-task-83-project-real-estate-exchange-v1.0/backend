package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Subscription;
import com.devcamp.realestateproject.repository.ISubscriptionRepo;

@RestController
@RequestMapping("/subscriptions")
@CrossOrigin
public class SubscriptionController {
    @Autowired
    private ISubscriptionRepo subscriptionRepo;

    @GetMapping
    public ResponseEntity<List<Subscription>> getAllSubscriptions() {
        try {
            List<Subscription> subscriptions = subscriptionRepo.findAll();
            return ResponseEntity.ok(subscriptions);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subscription> getSubscriptionById(@PathVariable("id") int id) {
        try {
            Optional<Subscription> subscription = subscriptionRepo.findById(id);
            return subscription.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Subscription> createSubscription(@RequestBody Subscription subscription) {
        try {
            Subscription createdSubscription = subscriptionRepo.save(subscription);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdSubscription);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Subscription> updateSubscription(@PathVariable("id") int id,
            @RequestBody Subscription subscription) {
        try {
            Optional<Subscription> existingSubscription = subscriptionRepo.findById(id);
            if (existingSubscription.isPresent()) {
                Subscription updatedSubscription = existingSubscription.get();
                updatedSubscription.setUser(subscription.getUser());
                updatedSubscription.setEndpoint(subscription.getEndpoint());
                updatedSubscription.setPublicKey(subscription.getPublicKey());
                updatedSubscription.setAuthenticationToken(subscription.getAuthenticationToken());
                updatedSubscription.setContentEncoding(subscription.getContentEncoding());
                Subscription savedSubscription = subscriptionRepo.save(updatedSubscription);
                return ResponseEntity.ok(savedSubscription);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSubscription(@PathVariable("id") int id) {
        try {
            if (subscriptionRepo.existsById(id)) {
                subscriptionRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
