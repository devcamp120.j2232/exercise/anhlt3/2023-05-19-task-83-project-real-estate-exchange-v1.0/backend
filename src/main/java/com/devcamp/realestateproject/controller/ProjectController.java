package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Project;
import com.devcamp.realestateproject.repository.IProjectRepo;

@RestController
@RequestMapping("/projects")
@CrossOrigin
public class ProjectController {
    @Autowired
    private IProjectRepo projectRepo;

    @GetMapping
    public ResponseEntity<List<Project>> getAllProjects() {
        try {
            List<Project> projects = projectRepo.findAll();
            return ResponseEntity.ok(projects);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable("id") int id) {
        try {
            Optional<Project> project = projectRepo.findById(id);
            return project.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Project> createProject(@RequestBody Project project) {
        try {
            Project createdProject = projectRepo.save(project);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdProject);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Project> updateProject(@PathVariable("id") int id, @RequestBody Project project) {
        try {
            Optional<Project> existingProject = projectRepo.findById(id);
            if (existingProject.isPresent()) {
                Project updatedProject = existingProject.get();
                updatedProject.setName(project.getName());
                updatedProject.setProvinceId(project.getProvinceId());
                updatedProject.setDistrictId(project.getDistrictId());
                updatedProject.setWardId(project.getWardId());
                updatedProject.setStreetId(project.getStreetId());
                updatedProject.setAddress(project.getAddress());
                updatedProject.setSlogan(project.getSlogan());
                updatedProject.setDescription(project.getDescription());
                updatedProject.setAcreage(project.getAcreage());
                updatedProject.setConstructArea(project.getConstructArea());
                updatedProject.setNumBlock(project.getNumBlock());
                updatedProject.setNumFloors(project.getNumFloors());
                updatedProject.setNumApartment(project.getNumApartment());
                updatedProject.setApartmentArea(project.getApartmentArea());
                updatedProject.setInvestor(project.getInvestor());
                updatedProject.setConstructionContractor(project.getConstructionContractor());
                updatedProject.setDesignUnit(project.getDesignUnit());
                updatedProject.setUtilities(project.getUtilities());
                updatedProject.setRegionLink(project.getRegionLink());
                updatedProject.setPhoto(project.getPhoto());
                updatedProject.setLat(project.getLat());
                updatedProject.setLng(project.getLng());
                Project savedProject = projectRepo.save(updatedProject);
                return ResponseEntity.ok(savedProject);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProject(@PathVariable("id") int id) {
        try {
            if (projectRepo.existsById(id)) {
                projectRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
