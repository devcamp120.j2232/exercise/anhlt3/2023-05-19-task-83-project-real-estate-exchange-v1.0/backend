package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Ward;
import com.devcamp.realestateproject.repository.IWardRepo;

@RestController
@RequestMapping("/wards")
@CrossOrigin
public class WardController {
    @Autowired
    private IWardRepo wardRepo;

    @GetMapping
    public ResponseEntity<Page<Ward>> getAllWards(Pageable pageable) {
        try {
            Page<Ward> wards = wardRepo.findAll(pageable);
            return ResponseEntity.ok(wards);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Ward> getWardById(@PathVariable("id") int id) {
        try {
            Optional<Ward> ward = wardRepo.findById(id);
            return ward.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Ward> createWard(@RequestBody Ward ward) {
        try {
            Ward createdWard = wardRepo.save(ward);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdWard);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Ward> updateWard(@PathVariable("id") int id, @RequestBody Ward ward) {
        try {
            Optional<Ward> existingWard = wardRepo.findById(id);
            if (existingWard.isPresent()) {
                Ward updatedWard = existingWard.get();
                updatedWard.setName(ward.getName());
                updatedWard.setPrefix(ward.getPrefix());
                updatedWard.setProvinceId(ward.getProvinceId());
                updatedWard.setDistrictId(ward.getDistrictId());
                Ward savedWard = wardRepo.save(updatedWard);
                return ResponseEntity.ok(savedWard);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteWard(@PathVariable("id") int id) {
        try {
            if (wardRepo.existsById(id)) {
                wardRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/prefix")
    public ResponseEntity<List<String>> getAllPrefix() {
        try {
            List<String> prefixes = wardRepo.findAllPrefix();
            return new ResponseEntity<>(prefixes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
