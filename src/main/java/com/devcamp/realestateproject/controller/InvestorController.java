package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Investor;
import com.devcamp.realestateproject.repository.IInvestorRepo;

@RestController
@RequestMapping("/investors")
@CrossOrigin
public class InvestorController {
    @Autowired
    private IInvestorRepo investorRepo;

    @GetMapping
    public ResponseEntity<List<Investor>> getAllInvestors() {
        try {
            List<Investor> investors = investorRepo.findAll();
            return ResponseEntity.ok(investors);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Investor> getInvestorById(@PathVariable("id") int id) {
        try {
            Optional<Investor> investor = investorRepo.findById(id);
            return investor.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Investor> createInvestor(@RequestBody Investor investor) {
        try {
            Investor createdInvestor = investorRepo.save(investor);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdInvestor);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Investor> updateInvestor(@PathVariable("id") int id, @RequestBody Investor investor) {
        try {
            Optional<Investor> existingInvestor = investorRepo.findById(id);
            if (existingInvestor.isPresent()) {
                Investor updatedInvestor = existingInvestor.get();
                updatedInvestor.setName(investor.getName());
                updatedInvestor.setDescription(investor.getDescription());
                updatedInvestor.setProjects(investor.getProjects());
                updatedInvestor.setAddress(investor.getAddress());
                updatedInvestor.setPhone(investor.getPhone());
                updatedInvestor.setPhone2(investor.getPhone2());
                updatedInvestor.setFax(investor.getFax());
                updatedInvestor.setEmail(investor.getEmail());
                updatedInvestor.setWebsite(investor.getWebsite());
                updatedInvestor.setNote(investor.getNote());
                Investor savedInvestor = investorRepo.save(updatedInvestor);
                return ResponseEntity.ok(savedInvestor);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteInvestor(@PathVariable("id") int id) {
        try {
            if (investorRepo.existsById(id)) {
                investorRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
