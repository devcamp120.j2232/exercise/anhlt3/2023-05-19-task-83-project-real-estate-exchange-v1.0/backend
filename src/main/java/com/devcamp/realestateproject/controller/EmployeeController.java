package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Employee;
import com.devcamp.realestateproject.repository.IEmployeeRepo;

@RestController
@RequestMapping("/employees")
@CrossOrigin
public class EmployeeController {
    @Autowired
    private IEmployeeRepo employeeRepo;

    @GetMapping
    public ResponseEntity<List<Employee>> getAllEmployees() {
        try {
            List<Employee> employees = employeeRepo.findAll();
            return ResponseEntity.ok(employees);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id) {
        try {
            Optional<Employee> employee = employeeRepo.findById(id);
            return employee.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        try {
            Employee createdEmployee = employeeRepo.save(employee);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdEmployee);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {
        try {
            Optional<Employee> existingEmployee = employeeRepo.findById(id);
            if (existingEmployee.isPresent()) {
                Employee updatedEmployee = existingEmployee.get();
                updatedEmployee.setLastName(employee.getLastName());
                updatedEmployee.setFirstName(employee.getFirstName());
                updatedEmployee.setTitle(employee.getTitle());
                updatedEmployee.setTitleOfCourtesy(employee.getTitleOfCourtesy());
                updatedEmployee.setBirthDate(employee.getBirthDate());
                updatedEmployee.setHireDate(employee.getHireDate());
                updatedEmployee.setAddress(employee.getAddress());
                updatedEmployee.setCity(employee.getCity());
                updatedEmployee.setRegion(employee.getRegion());
                updatedEmployee.setPostalCode(employee.getPostalCode());
                updatedEmployee.setCountry(employee.getCountry());
                updatedEmployee.setHomePhone(employee.getHomePhone());
                updatedEmployee.setExtension(employee.getExtension());
                updatedEmployee.setPhoto(employee.getPhoto());
                updatedEmployee.setNotes(employee.getNotes());
                updatedEmployee.setReportsTo(employee.getReportsTo());
                updatedEmployee.setUsername(employee.getUsername());
                updatedEmployee.setPassword(employee.getPassword());
                updatedEmployee.setEmail(employee.getEmail());
                updatedEmployee.setActivated(employee.getActivated());
                updatedEmployee.setProfile(employee.getProfile());
                updatedEmployee.setUserLevel(employee.getUserLevel());
                Employee savedEmployee = employeeRepo.save(updatedEmployee);
                return ResponseEntity.ok(savedEmployee);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable("id") int id) {
        try {
            if (employeeRepo.existsById(id)) {
                employeeRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
