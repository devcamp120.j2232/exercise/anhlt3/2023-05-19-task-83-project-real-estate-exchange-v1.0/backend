package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Province;
import com.devcamp.realestateproject.repository.IProvinceRepo;

@RestController
@RequestMapping("/provinces")
@CrossOrigin
public class ProvinceController {
    @Autowired
    private IProvinceRepo provinceRepo;

    @GetMapping
    public ResponseEntity<List<Province>> getAllProvinces() {
        try {
            List<Province> provinces = provinceRepo.findAll();
            return ResponseEntity.ok(provinces);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Province> getProvinceById(@PathVariable("id") int id) {
        try {
            Optional<Province> province = provinceRepo.findById(id);
            return province.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Province> createProvince(@RequestBody Province province) {
        try {
            Province createdProvince = provinceRepo.save(province);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdProvince);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Province> updateProvince(@PathVariable("id") int id, @RequestBody Province province) {
        try {
            Optional<Province> existingProvince = provinceRepo.findById(id);
            if (existingProvince.isPresent()) {
                Province updatedProvince = existingProvince.get();
                updatedProvince.setName(province.getName());
                updatedProvince.setCode(province.getCode());
                Province savedProvince = provinceRepo.save(updatedProvince);
                return ResponseEntity.ok(savedProvince);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProvince(@PathVariable("id") int id) {
        try {
            if (provinceRepo.existsById(id)) {
                provinceRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
