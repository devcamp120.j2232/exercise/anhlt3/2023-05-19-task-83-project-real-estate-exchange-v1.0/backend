package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.MasterLayout;
import com.devcamp.realestateproject.repository.IMasterLayoutRepo;

@RestController
@RequestMapping("/master-layouts")
@CrossOrigin
public class MasterLayoutController {
    @Autowired
    private IMasterLayoutRepo masterLayoutRepo;

    @GetMapping
    public ResponseEntity<List<MasterLayout>> getAllMasterLayouts() {
        try {
            List<MasterLayout> masterLayouts = masterLayoutRepo.findAll();
            return ResponseEntity.ok(masterLayouts);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable("id") int id) {
        try {
            Optional<MasterLayout> masterLayout = masterLayoutRepo.findById(id);
            return masterLayout.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<MasterLayout> createMasterLayout(@RequestBody MasterLayout masterLayout) {
        try {
            MasterLayout createdMasterLayout = masterLayoutRepo.save(masterLayout);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdMasterLayout);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<MasterLayout> updateMasterLayout(@PathVariable("id") int id,
            @RequestBody MasterLayout masterLayout) {
        try {
            Optional<MasterLayout> existingMasterLayout = masterLayoutRepo.findById(id);
            if (existingMasterLayout.isPresent()) {
                MasterLayout updatedMasterLayout = existingMasterLayout.get();
                updatedMasterLayout.setName(masterLayout.getName());
                updatedMasterLayout.setDescription(masterLayout.getDescription());
                updatedMasterLayout.setProjectId(masterLayout.getProjectId());
                updatedMasterLayout.setAcreage(masterLayout.getAcreage());
                updatedMasterLayout.setApartmentList(masterLayout.getApartmentList());
                updatedMasterLayout.setPhoto(masterLayout.getPhoto());
                MasterLayout savedMasterLayout = masterLayoutRepo.save(updatedMasterLayout);
                return ResponseEntity.ok(savedMasterLayout);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMasterLayout(@PathVariable("id") int id) {
        try {
            if (masterLayoutRepo.existsById(id)) {
                masterLayoutRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
