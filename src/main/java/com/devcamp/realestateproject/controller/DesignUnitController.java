package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.DesignUnit;
import com.devcamp.realestateproject.repository.IDesignUnitRepo;

@RestController
@RequestMapping("/design-units")
@CrossOrigin
public class DesignUnitController {
    @Autowired
    private IDesignUnitRepo designUnitRepo;

    @GetMapping
    public ResponseEntity<List<DesignUnit>> getAllDesignUnits() {
        try {
            List<DesignUnit> designUnits = designUnitRepo.findAll();
            return ResponseEntity.ok(designUnits);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<DesignUnit> getDesignUnitById(@PathVariable("id") int id) {
        try {
            Optional<DesignUnit> designUnit = designUnitRepo.findById(id);
            return designUnit.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<DesignUnit> createDesignUnit(@RequestBody DesignUnit designUnit) {
        try {
            DesignUnit createdDesignUnit = designUnitRepo.save(designUnit);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdDesignUnit);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<DesignUnit> updateDesignUnit(@PathVariable("id") int id, @RequestBody DesignUnit designUnit) {
        try {
            Optional<DesignUnit> existingDesignUnit = designUnitRepo.findById(id);
            if (existingDesignUnit.isPresent()) {
                DesignUnit updatedDesignUnit = existingDesignUnit.get();
                updatedDesignUnit.setName(designUnit.getName());
                updatedDesignUnit.setDescription(designUnit.getDescription());
                updatedDesignUnit.setProjects(designUnit.getProjects());
                updatedDesignUnit.setAddress(designUnit.getAddress());
                updatedDesignUnit.setPhone(designUnit.getPhone());
                updatedDesignUnit.setPhone2(designUnit.getPhone2());
                updatedDesignUnit.setFax(designUnit.getFax());
                updatedDesignUnit.setEmail(designUnit.getEmail());
                updatedDesignUnit.setWebsite(designUnit.getWebsite());
                updatedDesignUnit.setNote(designUnit.getNote());
                DesignUnit savedDesignUnit = designUnitRepo.save(updatedDesignUnit);
                return ResponseEntity.ok(savedDesignUnit);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDesignUnit(@PathVariable("id") int id) {
        try {
            if (designUnitRepo.existsById(id)) {
                designUnitRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
