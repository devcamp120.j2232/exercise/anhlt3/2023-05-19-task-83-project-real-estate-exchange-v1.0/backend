package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.ConstructionContractor;
import com.devcamp.realestateproject.repository.IConstructionContractorRepo;

@RestController
@RequestMapping("/construction-contractors")
@CrossOrigin
public class ConstructionContractorController {
    @Autowired
    private IConstructionContractorRepo contractorRepo;

    @GetMapping
    public ResponseEntity<List<ConstructionContractor>> getAllContractors() {
        try {
            List<ConstructionContractor> contractors = contractorRepo.findAll();
            return ResponseEntity.ok(contractors);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ConstructionContractor> getContractorById(@PathVariable("id") int id) {
        try {
            Optional<ConstructionContractor> contractor = contractorRepo.findById(id);
            return contractor.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<ConstructionContractor> createContractor(@RequestBody ConstructionContractor contractor) {
        try {
            ConstructionContractor createdContractor = contractorRepo.save(contractor);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdContractor);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ConstructionContractor> updateContractor(@PathVariable("id") int id,
            @RequestBody ConstructionContractor contractor) {
        try {
            Optional<ConstructionContractor> existingContractor = contractorRepo.findById(id);
            if (existingContractor.isPresent()) {
                ConstructionContractor updatedContractor = existingContractor.get();
                updatedContractor.setName(contractor.getName());
                updatedContractor.setDescription(contractor.getDescription());
                updatedContractor.setProjects(contractor.getProjects());
                updatedContractor.setAddress(contractor.getAddress());
                updatedContractor.setPhone(contractor.getPhone());
                updatedContractor.setPhone2(contractor.getPhone2());
                updatedContractor.setFax(contractor.getFax());
                updatedContractor.setEmail(contractor.getEmail());
                updatedContractor.setWebsite(contractor.getWebsite());
                updatedContractor.setNote(contractor.getNote());
                ConstructionContractor savedContractor = contractorRepo.save(updatedContractor);
                return ResponseEntity.ok(savedContractor);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteContractor(@PathVariable("id") int id) {
        try {
            if (contractorRepo.existsById(id)) {
                contractorRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
