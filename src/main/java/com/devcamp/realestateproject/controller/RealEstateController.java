package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.RealEstate;
import com.devcamp.realestateproject.repository.IRealEstateRepo;

@RestController
@RequestMapping("/real-estates")
@CrossOrigin
public class RealEstateController {
    @Autowired
    private IRealEstateRepo realEstateRepo;

    @GetMapping
    public ResponseEntity<List<RealEstate>> getAllRealEstates() {
        try {
            List<RealEstate> realEstates = realEstateRepo.findAll();
            return ResponseEntity.ok(realEstates);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<RealEstate> getRealEstateById(@PathVariable("id") int id) {
        try {
            Optional<RealEstate> realEstate = realEstateRepo.findById(id);
            return realEstate.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<RealEstate> createRealEstate(@RequestBody RealEstate realEstate) {
        try {
            RealEstate createdRealEstate = realEstateRepo.save(realEstate);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdRealEstate);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<RealEstate> updateRealEstate(@PathVariable("id") int id, @RequestBody RealEstate realEstate) {
        try {
            Optional<RealEstate> existingRealEstate = realEstateRepo.findById(id);
            if (existingRealEstate.isPresent()) {
                RealEstate updatedRealEstate = existingRealEstate.get();
                updatedRealEstate.setTitle(realEstate.getTitle());
                updatedRealEstate.setType(realEstate.getType());
                updatedRealEstate.setRequest(realEstate.getRequest());
                updatedRealEstate.setProvinceId(realEstate.getProvinceId());
                updatedRealEstate.setDistrictId(realEstate.getDistrictId());
                updatedRealEstate.setWardId(realEstate.getWardId());
                updatedRealEstate.setStreetId(realEstate.getStreetId());
                updatedRealEstate.setProjectId(realEstate.getProjectId());
                updatedRealEstate.setAddress(realEstate.getAddress());
                updatedRealEstate.setCustomerId(realEstate.getCustomerId());
                updatedRealEstate.setPrice(realEstate.getPrice());
                updatedRealEstate.setPriceMin(realEstate.getPriceMin());
                updatedRealEstate.setPriceTime(realEstate.getPriceTime());
                updatedRealEstate.setAcreage(realEstate.getAcreage());
                updatedRealEstate.setDirection(realEstate.getDirection());
                updatedRealEstate.setTotalFloors(realEstate.getTotalFloors());
                updatedRealEstate.setFloorNumber(realEstate.getFloorNumber());
                updatedRealEstate.setBath(realEstate.getBath());
                updatedRealEstate.setApartmentCode(realEstate.getApartmentCode());
                updatedRealEstate.setWallArea(realEstate.getWallArea());
                updatedRealEstate.setBedroom(realEstate.getBedroom());
                updatedRealEstate.setBalcony(realEstate.getBalcony());
                updatedRealEstate.setLandscapeView(realEstate.getLandscapeView());
                updatedRealEstate.setApartmentLocation(realEstate.getApartmentLocation());
                updatedRealEstate.setApartmentType(realEstate.getApartmentType());
                updatedRealEstate.setFurnitureType(realEstate.getFurnitureType());
                updatedRealEstate.setPriceRent(realEstate.getPriceRent());
                updatedRealEstate.setReturnRate(realEstate.getReturnRate());
                updatedRealEstate.setLegalDocument(realEstate.getLegalDocument());
                updatedRealEstate.setDescription(realEstate.getDescription());
                updatedRealEstate.setWidth(realEstate.getWidth());
                updatedRealEstate.setLength(realEstate.getLength());
                updatedRealEstate.setIsStreetHouse(realEstate.getIsStreetHouse());
                updatedRealEstate.setIsFBSO(realEstate.getIsFBSO());
                updatedRealEstate.setViewNumber(realEstate.getViewNumber());
                updatedRealEstate.setCreatedBy(realEstate.getCreatedBy());
                updatedRealEstate.setUpdatedBy(realEstate.getUpdatedBy());
                updatedRealEstate.setShape(realEstate.getShape());
                updatedRealEstate.setDistanceToFacade(realEstate.getDistanceToFacade());
                updatedRealEstate.setAdjacentFacadeNumber(realEstate.getAdjacentFacadeNumber());
                updatedRealEstate.setAdjacentRoad(realEstate.getAdjacentRoad());
                updatedRealEstate.setAlleyMinWidth(realEstate.getAlleyMinWidth());
                updatedRealEstate.setAdjacentAlleyMinWidth(realEstate.getAdjacentAlleyMinWidth());
                updatedRealEstate.setFactor(realEstate.getFactor());
                updatedRealEstate.setStructure(realEstate.getStructure());
                updatedRealEstate.setDTSXD(realEstate.getDTSXD());
                updatedRealEstate.setCLCL(realEstate.getCLCL());
                updatedRealEstate.setCTXDPrice(realEstate.getCTXDPrice());
                updatedRealEstate.setCTXDValue(realEstate.getCTXDValue());
                updatedRealEstate.setPhoto(realEstate.getPhoto());
                updatedRealEstate.setLat(realEstate.getLat());
                updatedRealEstate.setLng(realEstate.getLng());
                RealEstate savedRealEstate = realEstateRepo.save(updatedRealEstate);
                return ResponseEntity.ok(savedRealEstate);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRealEstate(@PathVariable("id") int id) {
        try {
            if (realEstateRepo.existsById(id)) {
                realEstateRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
