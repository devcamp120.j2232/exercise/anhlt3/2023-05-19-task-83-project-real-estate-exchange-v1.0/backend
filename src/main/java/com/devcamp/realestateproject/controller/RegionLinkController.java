package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.RegionLink;
import com.devcamp.realestateproject.repository.IRegionLinkRepo;

@RestController
@RequestMapping("/region-links")
@CrossOrigin
public class RegionLinkController {
    @Autowired
    private IRegionLinkRepo regionLinkRepo;

    @GetMapping
    public ResponseEntity<List<RegionLink>> getAllRegionLinks() {
        try {
            List<RegionLink> regionLinks = regionLinkRepo.findAll();
            return ResponseEntity.ok(regionLinks);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable("id") int id) {
        try {
            Optional<RegionLink> regionLink = regionLinkRepo.findById(id);
            return regionLink.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<RegionLink> createRegionLink(@RequestBody RegionLink regionLink) {
        try {
            RegionLink createdRegionLink = regionLinkRepo.save(regionLink);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdRegionLink);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<RegionLink> updateRegionLink(@PathVariable("id") int id, @RequestBody RegionLink regionLink) {
        try {
            Optional<RegionLink> existingRegionLink = regionLinkRepo.findById(id);
            if (existingRegionLink.isPresent()) {
                RegionLink updatedRegionLink = existingRegionLink.get();
                updatedRegionLink.setName(regionLink.getName());
                updatedRegionLink.setDescription(regionLink.getDescription());
                updatedRegionLink.setPhoto(regionLink.getPhoto());
                updatedRegionLink.setAddress(regionLink.getAddress());
                updatedRegionLink.setLat(regionLink.getLat());
                updatedRegionLink.setLng(regionLink.getLng());
                RegionLink savedRegionLink = regionLinkRepo.save(updatedRegionLink);
                return ResponseEntity.ok(savedRegionLink);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRegionLink(@PathVariable("id") int id) {
        try {
            if (regionLinkRepo.existsById(id)) {
                regionLinkRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
