package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Street;
import com.devcamp.realestateproject.repository.IStreetRepo;

@RestController
@RequestMapping("/streets")
@CrossOrigin
public class StreetController {
    @Autowired
    private IStreetRepo streetRepo;

    @GetMapping
    public ResponseEntity<Page<Street>> getAllStreets(Pageable pageable) {
        try {
            Page<Street> streetsPage = streetRepo.findAll(pageable);
            return ResponseEntity.ok(streetsPage);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Street> getStreetById(@PathVariable("id") int id) {
        try {
            Optional<Street> street = streetRepo.findById(id);
            return street.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Street> createStreet(@RequestBody Street street) {
        try {
            Street createdStreet = streetRepo.save(street);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdStreet);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Street> updateStreet(@PathVariable("id") int id, @RequestBody Street street) {
        try {
            Optional<Street> existingStreet = streetRepo.findById(id);
            if (existingStreet.isPresent()) {
                Street updatedStreet = existingStreet.get();
                updatedStreet.setName(street.getName());
                updatedStreet.setPrefix(street.getPrefix());
                updatedStreet.setProvinceId(street.getProvinceId());
                updatedStreet.setDistrictId(street.getDistrictId());
                Street savedStreet = streetRepo.save(updatedStreet);
                return ResponseEntity.ok(savedStreet);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteStreet(@PathVariable("id") int id) {
        try {
            if (streetRepo.existsById(id)) {
                streetRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/prefix")
    public ResponseEntity<List<String>> getAllPrefix() {
        try {
            List<String> prefixes = streetRepo.findAllPrefix();
            return new ResponseEntity<>(prefixes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
