package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.Utility;
import com.devcamp.realestateproject.repository.IUtilityRepo;

@RestController
@RequestMapping("/unilities")
@CrossOrigin
public class UtilityController {
    @Autowired
    private IUtilityRepo utilityRepo;

    @GetMapping
    public ResponseEntity<List<Utility>> getAllUtilities() {
        try {
            List<Utility> utilities = utilityRepo.findAll();
            return ResponseEntity.ok(utilities);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Utility> getUtilityById(@PathVariable("id") int id) {
        try {
            Optional<Utility> utility = utilityRepo.findById(id);
            return utility.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<Utility> createUtility(@RequestBody Utility utility) {
        try {
            Utility createdUtility = utilityRepo.save(utility);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdUtility);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Utility> updateUtility(@PathVariable("id") int id, @RequestBody Utility utility) {
        try {
            Optional<Utility> existingUtility = utilityRepo.findById(id);
            if (existingUtility.isPresent()) {
                Utility updatedUtility = existingUtility.get();
                updatedUtility.setName(utility.getName());
                updatedUtility.setDescription(utility.getDescription());
                updatedUtility.setPhoto(utility.getPhoto());
                Utility savedUtility = utilityRepo.save(updatedUtility);
                return ResponseEntity.ok(savedUtility);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUtility(@PathVariable("id") int id) {
        try {
            if (utilityRepo.existsById(id)) {
                utilityRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
