package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.AddressMap;
import com.devcamp.realestateproject.repository.IAddressMapRepo;

@RestController
@RequestMapping("/address-maps")
@CrossOrigin
public class AddressMapController {
    @Autowired
    private IAddressMapRepo addressMapRepo;

    @GetMapping
    public ResponseEntity<List<AddressMap>> getAllAddressMaps() {
        try {
            List<AddressMap> addressMaps = addressMapRepo.findAll();
            return ResponseEntity.ok(addressMaps);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable("id") int id) {
        try {
            Optional<AddressMap> addressMap = addressMapRepo.findById(id);
            return addressMap.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<AddressMap> createAddressMap(@RequestBody AddressMap addressMap) {
        try {
            AddressMap createdAddressMap = addressMapRepo.save(addressMap);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdAddressMap);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<AddressMap> updateAddressMap(@PathVariable("id") int id, @RequestBody AddressMap addressMap) {
        try {
            Optional<AddressMap> existingAddressMap = addressMapRepo.findById(id);
            if (existingAddressMap.isPresent()) {
                AddressMap updatedAddressMap = existingAddressMap.get();
                updatedAddressMap.setAddress(addressMap.getAddress());
                updatedAddressMap.setLat(addressMap.getLat());
                updatedAddressMap.setLng(addressMap.getLng());
                AddressMap savedAddressMap = addressMapRepo.save(updatedAddressMap);
                return ResponseEntity.ok(savedAddressMap);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAddressMap(@PathVariable("id") int id) {
        try {
            if (addressMapRepo.existsById(id)) {
                addressMapRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
