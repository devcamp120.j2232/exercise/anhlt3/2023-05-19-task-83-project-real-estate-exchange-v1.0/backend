package com.devcamp.realestateproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateproject.model.District;
import com.devcamp.realestateproject.repository.IDistrictRepo;

@RestController
@RequestMapping("/districts")
@CrossOrigin
public class DistrictController {
    @Autowired
    private IDistrictRepo districtRepo;

    @GetMapping
    public ResponseEntity<List<District>> getAllDistricts() {
        try {
            List<District> districts = districtRepo.findAll();
            return ResponseEntity.ok(districts);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
        try {
            Optional<District> district = districtRepo.findById(id);
            return district.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<District> createDistrict(@RequestBody District district) {
        try {
            District createdDistrict = districtRepo.save(district);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdDistrict);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<District> updateDistrict(@PathVariable("id") int id, @RequestBody District district) {
        try {
            Optional<District> existingDistrict = districtRepo.findById(id);
            if (existingDistrict.isPresent()) {
                District updatedDistrict = existingDistrict.get();
                updatedDistrict.setName(district.getName());
                updatedDistrict.setPrefix(district.getPrefix());
                updatedDistrict.setProvinceId(district.getProvinceId());
                District savedDistrict = districtRepo.save(updatedDistrict);
                return ResponseEntity.ok(savedDistrict);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDistrict(@PathVariable("id") int id) {
        try {
            if (districtRepo.existsById(id)) {
                districtRepo.deleteById(id);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/prefix")
    public ResponseEntity<List<String>> getAllPrefix() {
        try {
            List<String> prefixes = districtRepo.findAllPrefix();
            return new ResponseEntity<>(prefixes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/{provinceId}")
    public ResponseEntity<List<District>> getDistrictsByProvinceId(@PathVariable("provinceId") int provinceId) {
        try {
            List<District> districts = districtRepo.findByProvinceId_Id(provinceId);
            return ResponseEntity.ok(districts);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
