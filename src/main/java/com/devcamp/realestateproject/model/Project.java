package com.devcamp.realestateproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import lombok.Data;
import javax.persistence.Id;

@Entity
@Table(name = "project")
@Data
public class Project {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name", length = 200)
  private String name;

  @Column(name = "_province_id")
  private Integer provinceId;

  @Column(name = "_district_id")
  private Integer districtId;

  @Column(name = "_ward_id")
  private Integer wardId;

  @Column(name = "_street_id")
  private Integer streetId;

  @Column(name = "address", length = 1000)
  private String address;

  @Column(name = "slogan")
  private String slogan;

  @Column(name = "description")
  private String description;

  @Column(name = "acreage", precision = 20, scale = 2)
  private Double acreage;

  @Column(name = "construct_area", precision = 20, scale = 2)
  private Double constructArea;

  @Column(name = "num_block")
  private Integer numBlock;

  @Column(name = "num_floors", length = 500)
  private String numFloors;

  @Column(name = "num_apartment", nullable = false)
  private Integer numApartment;

  @Column(name = "apartment_area", length = 500)
  private String apartmentArea;

  @Column(name = "investor", nullable = false)
  private Integer investor;

  @Column(name = "construction_contractor")
  private Integer constructionContractor;

  @Column(name = "`design unit`")
  private Integer designUnit;

  @Column(name = "utilities", length = 1000, nullable = false)
  private String utilities;

  @Column(name = "region_link", length = 1000, nullable = false)
  private String regionLink;

  @Column(name = "photo", length = 5000)
  private String photo;

  @Column(name = "_lat")
  private Double lat;

  @Column(name = "_lng")
  private Double lng;

}
