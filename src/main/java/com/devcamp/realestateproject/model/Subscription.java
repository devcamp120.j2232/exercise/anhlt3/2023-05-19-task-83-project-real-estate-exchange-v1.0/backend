package com.devcamp.realestateproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import lombok.Data;
import javax.persistence.Id;

@Entity
@Table(name = "subscriptions")
@Data
public class Subscription {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "user")
  private String user;

  @Column(name = "endpoint")
  private String endpoint;

  @Column(name = "publickey")
  private String publicKey;

  @Column(name = "authenticationtoken")
  private String authenticationToken;

  @Column(name = "contentencoding")
  private String contentEncoding;

}
