package com.devcamp.realestateproject.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name = "province")
@Data
public class Province {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name", length = 50)
  private String name;

  @Column(name = "_code", length = 20)
  private String code;

}
