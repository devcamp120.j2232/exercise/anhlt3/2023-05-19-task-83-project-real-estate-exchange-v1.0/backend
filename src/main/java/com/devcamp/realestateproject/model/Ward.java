package com.devcamp.realestateproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import lombok.Data;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "ward")
@Data
public class Ward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name", nullable = false, length = 50)
    private String name;

    @Column(name = "_prefix", length = 20)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "_province_id")
    private Province provinceId;

    @ManyToOne
    @JoinColumn(name = "_district_id")
    private District districtId;

}
