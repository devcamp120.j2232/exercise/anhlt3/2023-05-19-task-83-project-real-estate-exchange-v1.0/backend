package com.devcamp.realestateproject.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import javax.persistence.Id;

@Entity
@Table(name = "customers")
@Data
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "contact_name", length = 255)
  private String contactName;

  @Column(name = "contact_title", length = 30)
  private String contactTitle;

  @Column(name = "address", length = 200)
  private String address;

  @Column(name = "mobile", unique = true, length = 80)
  private String mobile;

  @Column(name = "email", unique = true)
  private String email;

  @Column(name = "note", length = 5000)
  private String note;

  @Column(name = "create_by")
  private Integer createBy;

  @Column(name = "update_by")
  private Integer updateBy;

  @Column(name = "create_date")
  @CreationTimestamp
  private Date createDate;

  @Column(name = "update_date")
  @UpdateTimestamp
  private Date updateDate;
}
