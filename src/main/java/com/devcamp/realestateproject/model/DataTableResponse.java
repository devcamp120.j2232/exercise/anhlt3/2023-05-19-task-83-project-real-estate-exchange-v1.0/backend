package com.devcamp.realestateproject.model;

import java.util.List;

import lombok.Data;

@Data
public class DataTableResponse<T> {
    public DataTableResponse(List<Street> content, long totalElements, int number, int size) {
    }
    private List<T> data;
    private long recordsTotal;
    private int draw;
    private int recordsFiltered;
}
