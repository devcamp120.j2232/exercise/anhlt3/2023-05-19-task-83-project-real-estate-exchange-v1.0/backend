package com.devcamp.realestateproject.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;
import javax.persistence.Id;

@Entity
@Table(name = "realestate")
@Data
public class RealEstate {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "title", length = 2000)
  private String title;

  @Column(name = "type")
  private Integer type;

  @Column(name = "request")
  private Integer request;

  @Column(name = "province_id")
  private Integer provinceId;

  @Column(name = "district_id")
  private Integer districtId;

  @Column(name = "wards_id")
  private Integer wardId;

  @Column(name = "street_id")
  private Integer streetId;

  @Column(name = "project_id")
  private Integer projectId;

  @Column(name = "address", length = 2000, nullable = false)
  private String address;

  @Column(name = "customer_id")
  private Integer customerId;

  @Column(name = "price")
  private Long price;

  @Column(name = "price_min")
  private Long priceMin;

  @Column(name = "price_time")
  private Integer priceTime;

  @Column(name = "date_create", nullable = false)
  @CreationTimestamp
  private Date createDate;

  @Column(name = "acreage")
  private Double acreage;

  @Column(name = "direction")
  private Integer direction;

  @Column(name = "total_floors")
  private Integer totalFloors;

  @Column(name = "number_floors")
  private Integer floorNumber;

  @Column(name = "bath")
  private Integer bath;

  @Column(name = "apart_code", length = 10)
  private String apartmentCode;

  @Column(name = "wall_area")
  private Double wallArea;

  @Column(name = "bedroom")
  private Integer bedroom;

  @Column(name = "balcony")
  private Integer balcony;

  @Column(name = "landscape_view", length = 255)
  private String landscapeView;

  @Column(name = "apart_loca")
  private Integer apartmentLocation;

  @Column(name = "apart_type")
  private Integer apartmentType;

  @Column(name = "furniture_type")
  private Integer furnitureType;

  @Column(name = "price_rent")
  private Integer priceRent;

  @Column(name = "return_rate")
  private Double returnRate;

  @Column(name = "legal_doc")
  private Integer legalDocument;

  @Column(name = "description", length = 2000)
  private String description;

  @Column(name = "width_y")
  private Integer width;

  @Column(name = "long_x")
  private Integer length;

  @Column(name = "street_house")
  private Integer isStreetHouse;

  @Column(name = "FSBO")
  private Integer isFBSO;

  @Column(name = "view_num")
  private Integer viewNumber;

  @Column(name = "create_by")
  private Integer createdBy;

  @Column(name = "update_by")
  private Integer updatedBy;

  @Column(name = "shape", length = 200)
  private String shape;

  @Column(name = "distance2facade")
  private Integer distanceToFacade;

  @Column(name = "adjacent_facade_num")
  private Integer adjacentFacadeNumber;

  @Column(name = "adjacent_road", length = 200)
  private String adjacentRoad;

  @Column(name = "alley_min_width")
  private Integer alleyMinWidth;

  @Column(name = "adjacent_alley_min_width")
  private Integer adjacentAlleyMinWidth;

  @Column(name = "factor")
  private Integer factor;

  @Column(name = "structure", length = 2000)
  private String structure;

  @Column(name = "DTSXD")
  private Integer DTSXD;

  @Column(name = "CLCL")
  private Integer CLCL;

  @Column(name = "CTXD_price")
  private Integer CTXDPrice;

  @Column(name = "CTXD_value")
  private int CTXDValue;

  @Column(name = "photo", length = 2000)
  private String photo;

  @Column(name = "_lat")
  private Double lat;

  @Column(name = "_lng")
  private Double lng;

}
