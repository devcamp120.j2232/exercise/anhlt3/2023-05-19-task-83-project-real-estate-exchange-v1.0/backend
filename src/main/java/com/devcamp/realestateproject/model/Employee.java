package com.devcamp.realestateproject.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import lombok.Data;
import javax.persistence.Id;

@Entity
@Table(name = "employees")
@Data
public class Employee {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "`EmployeeID`")
  private int id;

  @Column(name = "`LastName`", length = 20, nullable = false)
  private String lastName;

  @Column(name = "`FirstName`", length = 10, nullable = false)
  private String firstName;

  @Column(name = "`Title`", length = 30)
  private String title;

  @Column(name = "`TitleOfCourtesy`", length = 25)
  private String titleOfCourtesy;

  @Column(name = "`BirthDate`")
  private Date birthDate;

  @Column(name = "`HireDate`")
  private Date hireDate;

  @Column(name = "`Address`", length = 60)
  private String address;

  @Column(name = "`City`", length = 15)
  private String city;

  @Column(name = "`Region`", length = 15)
  private String region;

  @Column(name = "`PostalCode`", length = 10)
  private String postalCode;

  @Column(name = "`Country`", length = 15)
  private String country;

  @Column(name = "`HomePhone`", length = 24)
  private String homePhone;

  @Column(name = "`Extension`", length = 4)
  private String extension;

  @Column(name = "`Photo`", length = 50)
  private String photo;

  @Column(name = "`Notes`")
  private String notes;

  @Column(name = "`ReportsTo`")
  private Integer reportsTo;

  @Column(name = "`Username`", length = 50)
  private String username;

  @Column(name = "`Password`", length = 255)
  private String password;

  @Column(name = "`Email`", length = 255)
  private String email;

  @Column(name = "`Activated`", columnDefinition = "ENUM('Y', 'N')")
  private String activated;

  @Column(name = "`Profile`")
  private String profile;

  @Column(name = "`UserLevel`")
  private Integer userLevel;
}
