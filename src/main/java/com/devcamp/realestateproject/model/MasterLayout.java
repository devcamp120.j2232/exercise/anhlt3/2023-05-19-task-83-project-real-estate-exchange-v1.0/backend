package com.devcamp.realestateproject.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import javax.persistence.Id;

@Entity
@Table(name = "master_layout")
@Data
public class MasterLayout {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "description", length = 5000)
  private String description;

  @Column(name = "project_id", nullable = false)
  private Integer projectId;

  @Column(name = "acreage", precision = 12, scale = 2)
  private Double acreage;

  @Column(name = "apartment_list", length = 1000)
  private String apartmentList;

  @Column(name = "photo", length = 5000)
  private String photo;

  @Column(name = "date_create", nullable = false)
  @CreationTimestamp
  private Date createDate;

  @Column(name = "date_update")
  @UpdateTimestamp
  private Date updateDate;

}
