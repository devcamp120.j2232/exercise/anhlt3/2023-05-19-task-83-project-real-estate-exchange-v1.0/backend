package com.devcamp.realestateproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "`design unit`")
@Data
public class DesignUnit {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name", length = 1000, nullable = false)
  private String name;

  @Column(name = "description", length = 5000)
  private String description;

  @Column(name = "projects", length = 2000)
  private String projects;

  @Column(name = "address")
  private Integer address;

  @Column(name = "phone", length = 50)
  private String phone;

  @Column(name = "phone2", length = 50)
  private String phone2;

  @Column(name = "fax", length = 50)
  private String fax;

  @Column(name = "email", length = 200)
  private String email;

  @Column(name = "website", length = 1000)
  private String website;

  @Column(name = "note")
  private String note;
}
